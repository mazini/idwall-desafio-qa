const puppeteer = require("puppeteer");

const BASE_URL = "https://blog.idwall.co";

// const fixtures = {}

const basePuppeteerConfig = {
  headless: false,
  devtools: true,
  slowMo: 250
};

const puppeteerEmulateConfig = {
  viewport: {
    width: 1366,
    height: 768
  },
  userAgent: ""
};
describe("Home page idwall blog", function() {
  it("Try to link menu itens", async function() {
    const browser = await puppeteer.launch({
      ...basePuppeteerConfig,
      args: ["--incognito"]
    });

    const page = await browser.newPage();
    page.emulate(puppeteerEmulateConfig);
    await page.goto(BASE_URL);
    await page.waitFor(6000);

    await page.waitForSelector(".menu");
    const currentLinks = await page.evaluate(() => {
      let elements = Array.from(document.querySelectorAll(".menu a"));
      let links = elements.map(element => {
        return element.href;
      });
      return links;
    });

    async function clickLinks() {
      if (currentLinks.length) {
        const currentHref = currentLinks.shift();

        const newPage = await browser.newPage();

        try {
          await newPage.goto(currentHref);
        } catch (e) {
          throw new Error(
            `An Error ocurrend on try to access link ${currentHref}`
          );
        }
        await newPage.close();
        await clickLinks(currentLinks);
      } else {
        console.log("done!");
      }
    }

    await clickLinks(currentLinks);
    await browser.close();
  }, 200000);

  it("Try to click on post links", async function() {
    const browser = await puppeteer.launch({
      ...basePuppeteerConfig,
      args: ["--incognito"]
    });

    const page = await browser.newPage();
    page.emulate(puppeteerEmulateConfig);
    await page.goto(BASE_URL);
    await page.waitFor(6000);
    await page.waitForSelector(".thumbnail");

    const currentPosts = await page.evaluate(() => {
      let elements = Array.from(document.querySelectorAll(".thumbnail a"));
      let posts = elements.map(element => {
        return element.href;
      });
      return posts;
    });

    async function clickPosts() {
      if (currentPosts.length) {
        const currentPostHref = currentPosts.shift();

        const newPage = await browser.newPage();

        try {
          await newPage.goto(currentPostHref);
        } catch (e) {
          throw new Error(
            `An Error ocurrend on try to access link ${currentPostHref}`
          );
        }
        await newPage.close();
        await clickPosts(currentPosts);
      } else {
        console.log("done!");
      }
    }

    await clickPosts(currentPosts);
    await browser.close();
  }, 200000);

  it("Try to search some post", async function() {
    const browser = await puppeteer.launch({
      ...basePuppeteerConfig,
      args: ["--incognito"]
    });

    const page = await browser.newPage();
    page.emulate(puppeteerEmulateConfig);
    await page.goto(BASE_URL);
    await page.waitFor(6000);
    await page.waitForSelector(".search-click");

    await page.click(".search-click");
    await page.waitFor(2000);
    await page.focus(".search-input");
    await page.keyboard.type("lavagem de dinheiro");
    await page.keyboard.press("Enter");
    await page.waitFor(5000);

    await browser.close();
  }, 200000);
});