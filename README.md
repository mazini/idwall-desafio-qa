# idwall - QA Challenge

## Dependencies
- [Jest](https://jestjs.io/)
- [Puppeter](https://github.com/GoogleChrome/puppeteer)
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com)

## How to run

```sh
$ npm install && npm run test
``` 